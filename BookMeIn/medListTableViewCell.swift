//
//  medListTableViewCell.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class medListTableViewCell: UITableViewCell {

    @IBOutlet weak var medListImg: UIImageView!
    @IBOutlet weak var medListName: UILabel!
    @IBOutlet weak var medListRating: UILabel!
    @IBOutlet weak var medListopen: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
