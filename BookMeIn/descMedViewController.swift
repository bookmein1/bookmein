//
//  descMedViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/5/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
protocol setDateMedValueDelegate {
    func setDate(toValue: String)
}


class descMedViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var descMedImg: UIImageView!
    @IBOutlet weak var descNameMed: UILabel!
    @IBOutlet weak var descMedLoc: UILabel!
    @IBOutlet weak var descMedRating: UILabel!
    @IBOutlet weak var descMedOpen: UILabel!
    @IBOutlet weak var descMed: UILabel!
    @IBOutlet weak var descMedDatePic: UIDatePicker!
    @IBAction func descMedDateValue(_ sender: Any) {
        
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        descDateValueMed.text = formatter.string(from: descMedDatePic.date)
        
    }
    
    @IBOutlet weak var descDateValueMed: UILabel!
    @IBOutlet weak var descMedInput: UITextField!
    @IBAction func confirmButton(_ sender: Any) {
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        descMed.layer.masksToBounds = false
        descMed.layer.cornerRadius = descMed.frame.height/16
        descMed.clipsToBounds = true
        
        
        descNameMed.text = medNameTable[medServicesIndex]
        descMedRating.text = medRating[medServicesIndex]
        descMedOpen.text = medOpenHrs[medServicesIndex]
        descMedImg.image = UIImage(named: medImages[medServicesIndex])
        descMedLoc.text = medAdd[medServicesIndex]
        descMed.text = medDesc[medServicesIndex]
        
        
        descMedInput.delegate = self
        descMedInput.tag = 0
        descMedInput.returnKeyType = UIReturnKeyType.next

        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var delegate: setDateMedValueDelegate?
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: descMedDatePic.date)
        descDateValueMed.text = strDate
        
        
        if self.delegate != nil
        {
            self.delegate?.setDate(toValue: strDate)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let secondController = segue.destination as! confirmMedViewController
        secondController.peopleString = descMedInput.text!
        secondController.receivedDate = descDateValueMed.text!
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
