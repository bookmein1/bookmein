//
//  Auth.swift
//  BookMeIn
//
//

import Foundation
import FirebaseAuth

typealias LoginHandler = (_ msg: String?) -> Void;

struct LoginErrorCode {
    static let INVALID_EMAIL = "Invalid Email Address, Please Provide a Real Email Address";
    static let WRONG_PASSWORD = "Wrong Password, Please Enter The Correct Password";
    static let PROBLEM_CONNECTING = "Problem Connection to Database, Please try Again"
    static let USER_NOT_FOUND = "User Not Found, Please Register";
    static let EMAIL_ALREADY_IN_USE = "Email Already in Use, Please Use another Email";
    static let WEAK_PASSWORD = "Password Should be at least 6 Characters Long";
}

class AuthCheck {
    private static let _instance = AuthCheck();
    
    static var Instance: AuthCheck {
        return _instance;
    }
    
    func login(withEmail: String, password: String, loginHandler: LoginHandler?)  {
        
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            
            if error != nil {
                self.handleErrors(err: error! as NSError, loginHandler: loginHandler);
            } else {
                loginHandler?(nil);
            }
            
            });
            
    } //login function	
    
    func signUp(withEmail: String, password: String, loginHandler: LoginHandler?) {
        
        Auth.auth().createUser(withEmail: withEmail, password: password, completion: { (user, error) in
            
            if error != nil {
                self.handleErrors(err: error! as NSError, loginHandler: loginHandler);
            } else {
                
                if user?.uid != nil {
                    //store user to database
                    DBProvider.Instance.saveUser(withID: (user?.uid)!, email: withEmail, password: password)
                    //login user
                    self.login(withEmail: withEmail, password: password, loginHandler: loginHandler);
                }	
            }
        });
    } // signup function
    
    func logOut() -> Bool {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut();
                return true;
            } catch {
                return false;
            }
        }
        return true;
    }
    
        private func handleErrors(err: NSError, loginHandler: LoginHandler?) {
            
            if let errCode = AuthErrorCode(rawValue: err.code) {
                
                switch errCode {
                case .wrongPassword:
                    loginHandler?(LoginErrorCode.WRONG_PASSWORD);
                    break;
                case .invalidEmail:
                    loginHandler?(LoginErrorCode.INVALID_EMAIL);                    break;
                case .userNotFound:
                    loginHandler?(LoginErrorCode.USER_NOT_FOUND);                    break;
                case .emailAlreadyInUse:
                    loginHandler?(LoginErrorCode.EMAIL_ALREADY_IN_USE);                    break;
                case .weakPassword:
                    loginHandler?(LoginErrorCode.WEAK_PASSWORD);                    break;
                default:
                    loginHandler?(LoginErrorCode.PROBLEM_CONNECTING);                    break;
                
                }
           }
    }
}
