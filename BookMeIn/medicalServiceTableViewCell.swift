//
//  medicalServiceTableViewCell.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class medicalServiceTableViewCell: UITableViewCell {
    @IBOutlet weak var serviceImg: UIImageView!

    @IBOutlet weak var medOpenHrs: UILabel!
    @IBOutlet weak var medRating: UILabel!
    @IBOutlet weak var medName: UILabel!
    @IBOutlet weak var serviceMedName: UILabel!
    
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
