//
//  beautyTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
import Foundation

struct serviceBeauty {
    var serviceNameTable : [String]
}


var  beautyNames = ["Ultimate Hairworks and Beauty","Salon First","Black Swan Beauty","Starlet Hair and Beauty ","Nails Extension"]
var  beautyImgs = ["uhb","salonfirst","blackswan","starlet","hairextension"]
var  beautyRatings = ["★★★★★", "★★★★","★★★★★","★★★★★","★★★"]
var  beautyOpen = ["Opening hours: 10.00 - 17.00", "Opening hours: 09.00 - 17.00", "Opening hours: 10.00 - 17.00", "Opening hours: 09.00 - 16.00", "Opening hours: 09.00 - 17.30"]
var beautyAdd = ["4/2-8 Brwood Hwy, Burwood East VIC 3151","Unit 6 125 Highbury Rd, Burwood VIC 3125","5A Robinlee Ave, Burwood East VIC 3151","15 Sevenoaks Rd, Burwood East VIC 3151","g41/172-210 Burwood Hwy, Burwood East VIC 3151"]

var descBeautys = ["Ultimate Hairworks & Beauty offer a huge range of Hair, Face and Body Services and Treatments. We believe that our speciality is pampering you from top-to-toe. Our friendly and enthusiastic team of professional Beauty Therapists and Hairdressers are happy to attend to all your hair and beauty needs. ","Salon First is a wholesale salon supplier. Australian born & bred. We don’t sell to the general public or retail customers. If you would like to access the pricing and ordering information on our web site please contact us to become a member (it’s Free!). We will need to confirm your professional beauty status in order to do this. Or if you are already a Salon First customer, we will create a log in for you through the site, just email us & we’ll follow up with you.","Black Swan Beauty offers some good services, such as nails, waxing and tanning.","Starlet hair and beauty locates in BUrwood Highway. We offers some interesting services for the customers, such as Bleaching, blow Drying, Hair Coloring, Scalp Massages, and so on. Please come and use our services.","Nail Extension offers the services you need for a comfortable manicure and pedicure experience. Indulge in their serene atmosphere or get the express service you need. Their accomplished team provides a range of services including acrylic nails, gel nails and manicure. They are located conveniently in Burwood East. Call to make an appointment today. "]

var beautyIndex = 0


class beautyTableViewController: UITableViewController {

    var beautyServices = [serviceBeauty]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        beautyServices = [serviceBeauty(serviceNameTable: ["Manicures", "Pedicures","Shellac Nails","Spray Tanning","Hair styling","Human hair extensions"]),
                          serviceBeauty(serviceNameTable: ["Manicure & Pedicure","Nail Polish","Makeup","Tanning","Waxing"]),
                          serviceBeauty(serviceNameTable: ["Nails","Waxing","Lashes"]),
                          serviceBeauty(serviceNameTable: ["Bleaching","Blow Drying","Hair Coloring","Scalp Massages","Hair repair treatments"]),
                          serviceBeauty(serviceNameTable: ["Manicure","Acrylic Nails"," Gel nails"])
                        
        ]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return beautyNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "beautyList", for: indexPath) as! beautyMenuTableViewCell
        // Configure the cell...

        cell.beautyName?.text = beautyNames[indexPath.row]
        
        
        //text for restaurant ratings
        cell.beautyRating?.text = beautyRatings[indexPath.row]
        //text for restaurant opening hours
        cell.beautyOpenHrs?.text = beautyOpen[indexPath.row]
        //image for restaurant
        cell.beautyImg?.image = UIImage(named: beautyImgs[indexPath.row])

        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        beautyIndex = indexPath.row
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            let DestViewController = segue.destination as! descBeautyViewController
            var secondTableArrayTwo : serviceBeauty
            secondTableArrayTwo = beautyServices[indexPath.row]
            DestViewController.listServicesBeauty = secondTableArrayTwo.serviceNameTable
            
            
            
            
        }
    }
    
}
