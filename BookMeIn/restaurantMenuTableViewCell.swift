//
//  restaurantMenuTableViewCell.swift
//  BookMeIn
//
//

import UIKit


class restaurantMenuTableViewCell: UITableViewCell {

 
    @IBOutlet weak var imgList: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var openinghrsLabel: UILabel!
    @IBOutlet weak var starLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
