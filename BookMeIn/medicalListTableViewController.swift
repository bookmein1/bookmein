//
//  medicalListTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class medicalListTableViewController: UITableViewController {

    var medicalArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return medicalArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medList", for: indexPath) as! medListTableViewCell

        // Configure the cell...
        cell.medListName?.text = medicalArray[indexPath.row]
        cell.medListRating?.text = medicalArray[indexPath.row]
        cell.medListopen?.text = medicalArray[indexPath.row]
        cell.medListImg?.image = UIImage(named: medicalArray[indexPath.row])
        

        return cell
    }
 

    
}
