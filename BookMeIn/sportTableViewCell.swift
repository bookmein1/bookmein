//
//  sportTableViewCell.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class sportTableViewCell: UITableViewCell {

    @IBOutlet weak var sportImg: UIImageView!
    
    @IBOutlet weak var sportName: UILabel!
    
    
    @IBOutlet weak var sportRating: UILabel!
    
    
    @IBOutlet weak var sportOpenHrs: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
