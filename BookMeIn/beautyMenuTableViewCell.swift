//
//  beautyMenuTableViewCell.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class beautyMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var beautyRating: UILabel!
    @IBOutlet weak var beautyName: UILabel!
    @IBOutlet weak var beautyOpenHrs: UILabel!
    @IBOutlet weak var beautyImg: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
