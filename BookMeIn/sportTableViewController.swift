//
//  sportTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

var  sportNames = ["Burwood Fitness Centre","Aqualink Box Hill","Anytime Fitness"]
var  sportImgs = ["deakin","Aqualink","anytime"]
var  sportRatings = ["★★★★", "★★★★","★★★★"]
var  sportOpen = ["Opening hours: 06.00 – 21.00", "Opening hours: 06.00 - 22.00", "Opening hours: 24 Hours"]
var sportAdd = ["221 Burwood Hwy, Burwood VIC 3125","Surrey Drive Box Hill VIC 3128","85 Burwood Hwy, Burwood VIC 3125"]

var descSports = ["Deakin YMCA has catered to the health and fitness needs of the Deakin University Community since 2006, our centre is a community hub where people come to develop in Body, Mind and Spirit.","TAqualink Box Hill has set a new standard for aquatic and leisure facilities in Australia. From its leading-edge environmentally-sustainable design features to its spacious and flexible aquatic and dry area facilities, it has become one of the most modern and innovative centres in Australia.","It all starts with heart. Put it into everything you do and extraordinary things happen. Half a million people are changing their lives every day with Anytime Fitness. That’s 500,000 hearts beating as one. Join them."]

var sportIndex = 0

class sportTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sportNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sportList", for: indexPath)
        as! sportTableViewCell

        // Configure the cell...
        
        cell.sportName?.text = sportNames[indexPath.row]
        
        
        //text for restaurant ratings
        cell.sportRating?.text = sportRatings[indexPath.row]
        //text for restaurant opening hours
        cell.sportOpenHrs?.text = sportOpen[indexPath.row]
        //image for restaurant
        cell.sportImg?.image = UIImage(named: sportImgs[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        sportIndex = indexPath.row
        
        
    }
    
}
