//
//  searchTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/5/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class searchTableViewController: UITableViewController {

    var  sportNames = ["Burwood Fitness Centre"]
    var  sportImgs = ["deakin"]
    var  sportRatings = ["★★★★"]
    
    
    var sportIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sportNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchList", for: indexPath) as! searchTableViewCell

        // Configure the cell...
        cell.seachName?.text = sportNames[indexPath.row]
        
        
        //text for restaurant ratings
        cell.seachRating?.text = sportRatings[indexPath.row]
        //text for restaurant opening hours
        cell.searchOpen?.text = sportOpen[indexPath.row]
        //image for restaurant
        cell.searchImg?.image = UIImage(named: sportImgs[indexPath.row])


        return cell
    }

}
