//
//  descBeautyViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
protocol setDateBeautyValueDelegate {
    func setDate(toValue: String)
}


class descBeautyViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var listServicesBeauty = [String]()
    
    @IBOutlet weak var descBeautyImg: UIImageView!
    @IBOutlet weak var descBeautyName: UILabel!
    @IBOutlet weak var descBeautyAdd: UILabel!
    @IBOutlet weak var descBeautyRating: UILabel!
    @IBOutlet weak var descBeautyOpenhrs: UILabel!
    @IBOutlet weak var descBeauty: UILabel!
    @IBOutlet weak var servicePicker: UIPickerView!
    
    @IBOutlet weak var descDatePicker: UIDatePicker!
    
    @IBOutlet weak var labelService: UILabel!
    
    @IBAction func beautyDateValue(_ sender: Any) {
        
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        labelDate.text = formatter.string(from:descDatePicker.date)

        
    }
    
    
    
    @IBOutlet weak var labelDate: UILabel!
    @IBAction func confirmButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        descBeauty.layer.masksToBounds = false
        descBeauty.layer.cornerRadius = descBeauty.frame.height/16
        descBeauty.clipsToBounds = true
        
        
        descBeautyName.text = beautyNames[beautyIndex]
        descBeautyRating.text = beautyRatings[beautyIndex]
        descBeautyOpenhrs.text = beautyOpen[beautyIndex]
        descBeautyImg.image = UIImage(named: beautyImgs[beautyIndex])
        descBeautyAdd.text = beautyAdd[beautyIndex]
        descBeauty.text = descBeautys[beautyIndex]
        
        servicePicker.delegate = self
        servicePicker.dataSource = self
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var delegate: setDateBeautyValueDelegate?
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker.date)
        labelDate.text = strDate
        
        
        if self.delegate != nil
        {
            self.delegate?.setDate(toValue: strDate)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let thirdController = segue.destination as! confirmBeautyViewController
        
        thirdController.receivedBeautyDate = labelDate.text!
        thirdController.receivedBeautyService = labelService.text!
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listServicesBeauty[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listServicesBeauty.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        labelService.text = listServicesBeauty[row]
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
