//
//  descriptionViewController.swift
//  BookMeIn
//

//

import UIKit
protocol setDateValueDelegate {
    func setDate(toValue: String)
}



class descriptionViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var descImage: UIImageView!
    @IBOutlet weak var descTitle: UILabel!
    @IBOutlet weak var descStar: UILabel!
    @IBOutlet weak var descOpeninghrs: UILabel!
    @IBOutlet weak var descLocation: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    
    @IBAction func nextButton(_ sender: Any) {
    }
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func datePickerValue(_ sender: Any) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        timePicker.text = formatter.string(from: datePicker.date)
    }
    
    @IBOutlet weak var timePicker: UILabel!
    
    @IBOutlet weak var inputPeople: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        descriptionText.layer.masksToBounds = false
        descriptionText.layer.cornerRadius = descriptionText.frame.height/16
        descriptionText.clipsToBounds = true
        
        //These codes display an image, restaurant name, ratings, and opening hours, which obtained from previous page (restaurant list)
        descTitle.text = nameList[restIndex]
        descStar.text = starList[restIndex]
        descOpeninghrs.text = openHrsList[restIndex]
        descImage.image = UIImage(named: restImage[restIndex])
        descLocation.text = address[restIndex]
        descriptionText.text = descRestaurant[restIndex]
        
        inputPeople.delegate = self
      

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    var delegate: setDateValueDelegate?
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker.date)
        timePicker.text = strDate
        
        
        if self.delegate != nil
        {
            self.delegate?.setDate(toValue: strDate)
        }
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let secondController = segue.destination as! confirmViewController
        secondController.peopleString = inputPeople.text!
        secondController.receivedDate = timePicker.text!
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }

}
