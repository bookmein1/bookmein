//
//  confirmEduViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
protocol setDateEduValueDelegate {
    func setDate(toValue: String)
}




class confirmEduViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var descEduInput: UITextField!
    
    @IBOutlet weak var descEduImg: UIImageView!
    @IBOutlet weak var descNameEdu: UILabel!
    @IBOutlet weak var descEduLoc: UILabel!
    @IBOutlet weak var descEduRating: UILabel!
    @IBOutlet weak var descEduOpen: UILabel!
    @IBOutlet weak var descEdu: UILabel!
    @IBOutlet weak var descEduDatePicker: UIDatePicker!
    @IBAction func descEduDateValue(_ sender: Any) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        dateEdu.text = formatter.string(from: descEduDatePicker.date)
        
        
    }
    
    @IBAction func descConfirmEdu(_ sender: Any) {
    }
    
    @IBOutlet weak var dateEdu: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        descEdu.layer.masksToBounds = false
        descEdu.layer.cornerRadius = descEdu.frame.height/16
        descEdu.clipsToBounds = true
        
        descNameEdu.text = eduNames[eduIndex]
        descEduRating.text = eduRatings[eduIndex]
        descEduOpen.text = eduOpen[eduIndex]
        descEduImg.image = UIImage(named: eduImgs[eduIndex])
        descEduLoc.text = eduAdd[eduIndex]
        descEdu.text = descEdus[eduIndex]

        descEduInput.delegate = self
        descEduInput.tag = 0
        descEduInput.returnKeyType = UIReturnKeyType.next
       
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var delegate: setDateEduValueDelegate?
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker.date)
        dateEdu.text = strDate
        
        
        if self.delegate != nil
        {
            self.delegate?.setDate(toValue: strDate)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let secondController = segue.destination as! EduconfirmViewController
        secondController.peopleString = descEduInput.text!
        secondController.receivedDate = dateEdu.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
