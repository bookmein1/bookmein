//
//  confirmBeautyViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
import EventKit
import FirebaseAuth

class confirmBeautyViewController: UIViewController, setDateBeautyValueDelegate {

    
    @IBOutlet weak var confirmBeauty: UILabel!
    @IBOutlet weak var companyBeauty: UILabel!
    @IBOutlet weak var serviceBeauty: UILabel!
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var beautyLoc: UILabel!
    @IBAction func confirmButton(_ sender: Any) {
        
        displayAlert(userMessage: "Thank you for booking.")
       
        
    }
    
    var receivedBeautyDate:String = ""
    var receivedBeautyService: String = ""

    
    @IBAction func addCalendar(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .short
        let date = dateFormatter.date(from: receivedBeautyDate) //according to date format your date string
        let eventStore: EKEventStore = EKEventStore()
        
        eventStore.requestAccess (to: .event) {(granted, error) in
            if (granted) && (error == nil)
            {
                
                print("granted \(granted)")
                print("error \(String(describing: error))")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = "Reminder: " + self.companyBeauty.text!
                event.startDate = date!
                event.endDate = date!
                event.notes = "Notes: You have an appointment in " + self.companyBeauty.text!
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                }
                catch let error as NSError {
                    print("error: \(error)")
                }
                print ("Save Event")
            }
            else {
                print("error: \(String(describing: error))")
            }
            
        }
        
    }
    
    
    
    func displayAlert(userMessage:String)
    {
        
        let alertMessage = UIAlertController(title:"Booking confirmed!", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
 self.navigationController?.popToRootViewController(animated: true)
        alertMessage.addAction(okAction);
        
        present(alertMessage, animated:true, completion:nil);
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let goNextPage = storyBoard.instantiateViewController(withIdentifier: "categoriesPage")
        self.present(goNextPage, animated: true, completion: nil)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        companyBeauty.text = beautyNames[beautyIndex]
        timeValue.text = receivedBeautyDate
        beautyLoc.text = beautyAdd[beautyIndex]
        serviceBeauty.text = receivedBeautyService
        
        if Auth.auth().currentUser != nil {
            confirmBeauty.text = (Auth.auth().currentUser?.email)!
        } else {
            confirmBeauty.text = "Login ERROR"
        }
        
    }
    
    func setDate(toValue:String) {
        self.timeValue.text = toValue
    }
    
    func setServices(toValue:String) {
        self.serviceBeauty.text = toValue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
