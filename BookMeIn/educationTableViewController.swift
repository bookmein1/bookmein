//
//  educationTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

var  eduNames = ["PALM Burwood","Room Booking Deakin","Junior Holiday Program - RSPCA","Deakin IELTS Test Centre ","Hendersons Educational Service"]
var  eduImgs = ["palm","deakin","rspca","deakin","handerson"]
var  eduRatings = ["★★★★★", "★★★★","★★★★★","★★★★★","★★★"]
var  eduOpen = ["Opening hours: 08.00 - 17.00", "Opening hours: 08.00 - 17.00", "Opening hours: 09.30 - 16.30", "Opening hours: 08.30 – 16.00", "Opening hours: 09.30 – 17.30"]
var eduAdd = ["221 Burwood Hwy, Burwood VIC 3125","221 Burwood Hwy, Burwood VIC 3125","Burwood East","70 Elgar Rd, Burwood VIC 3125","735 Burwood Rd, Hawthorn East VIC 3123"]

var descEdus = ["PALMs (Peer Assisted Learning Mentors) are high-achieving Deakin College students who have volunteered their time to assist you with your studies. A PALM can help you by sharing their experiences at Deakin College and giving you useful tips and tools to improve your studies now and in the future.","As rooms are booked in line with the Academic Calendar, spaces may only be booked as far in advance as the University timetable. To ensure efficient use of space, space is booked on a needs basis and subject to availability. For teaching spaces, core University business (primarily teaching) takes priority.","Do you love animals and want to learn more about them? Our holiday program offers animal lovers of all ages a chance to get up close and personal with their favorite creatures.","IELTS is a test used around the world when a certain level of English is required for studying, working or living in another country. It tests listening, reading, writing and speaking skills.IELTS isn't just useful for getting into Deakin – it's accepted as evidence of English language skills by over 8000 organisations worldwide. ","We are specialists in exam preparation for scholarships to private schools, as well as entrance exams for accelerated learning (SEAL) and high achiever programs in Victorian high schools. We pride ourselves on the number of students we assist in obtaining places at Melbourne High, Mac. Robertson Girls’, Nossal High, Suzanne Cory High and John Monash Science School."]

var eduIndex = 0

class educationTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eduNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eduList", for: indexPath) as! educationTableViewCell

        // Configure the cell...
        cell.eduName?.text = eduNames[indexPath.row]
        
        
        //text for restaurant ratings
        cell.eduRating?.text = eduRatings[indexPath.row]
        //text for restaurant opening hours
        cell.eduOpenHrs?.text = eduOpen[indexPath.row]
        //image for restaurant
        cell.eduImg?.image = UIImage(named: eduImgs[indexPath.row])
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        eduIndex = indexPath.row
        
        
    }
    

    
}
