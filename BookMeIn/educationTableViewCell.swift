//
//  educationTableViewCell.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class educationTableViewCell: UITableViewCell {

    @IBOutlet weak var eduImg: UIImageView!
    @IBOutlet weak var eduName: UILabel!
    
    @IBOutlet weak var eduRating: UILabel!
    
    
    @IBOutlet weak var eduOpenHrs: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
