//
//  homepageViewController.swift
//  BookMeIn
//
//

import UIKit
import FirebaseAuth

class homepageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerImg.layer.masksToBounds = false
        headerImg.layer.cornerRadius = headerImg.frame.height/12
        headerImg.clipsToBounds = true

        
        
        // Do any additional setup after loading the view.
        
        
        if Auth.auth().currentUser != nil {
            UserLoggedIN.text = "Welcome " + (Auth.auth().currentUser?.email)!
        } else {
            UserLoggedIN.text = "Login ERROR"
        }
    }

    /*-----------------
 
 
    // let playButton  = UIButton(type: .custom)
    //playButton.setImage(UIImage(named: "play.png"), for: .normal)

    ---------
 
     override func viewDidLoad {
     ...
     playButton = UIButton(type: .Custom)
     imagePlay = UIImage(named:"play.png")
     playButton.setImage(imagePlay, forState: .Normal)
     }
 
 */
    
    @IBOutlet weak var headerImg: UIImageView!
    
    @IBOutlet weak var UserLoggedIN: UILabel!
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
