//
//  aboutViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/5/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit

class aboutViewController: UIViewController {

    
    @IBOutlet weak var descText: UILabel!
    @IBOutlet weak var gwendaImg: UIImageView!
    
    @IBOutlet weak var forrestImg: UIImageView!
    
    @IBOutlet weak var leonImg: UIImageView!
    @IBOutlet weak var mingkeImg: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descText.text = "Book Me In is an application based on iOS platform. This application offers the services to ease the users to book the appointment from the existing services. "
        
        gwendaImg.layer.masksToBounds = false
        gwendaImg.layer.cornerRadius = gwendaImg.frame.height/8
        gwendaImg.clipsToBounds = true
        
        forrestImg.layer.masksToBounds = false
        forrestImg.layer.cornerRadius = forrestImg.frame.height/8
        forrestImg.clipsToBounds = true
        
        leonImg.layer.masksToBounds = false
        leonImg.layer.cornerRadius = leonImg.frame.height/8
        leonImg.clipsToBounds = true
        
        mingkeImg.layer.masksToBounds = false
        mingkeImg.layer.cornerRadius = mingkeImg.frame.height/8
        mingkeImg.clipsToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
