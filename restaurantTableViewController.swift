//
//  restaurantTableViewController.swift
//  BookMeIn
//
//

import UIKit
import Firebase
import FirebaseDatabase

//struct postStruct {
//    let label : String!
//    let rating : String!
//    let openHrs : String!
//}
//var restIndex = 1
//var posts = [postStruct]()

var nameList = ["Mrs Robinson","Nando's Burwood","Nene Chicken Box Hill","Geppetto's","Taste Dumplings"]
var restImage = ["mrsrobinson","nandos","nenechicken","gepettos","tastedumpling"]
var starList = ["★★★★★", "★★★★","★★★★","★★★★","★★★★★"]
var openHrsList = ["Opening hours: 07.00 - 18.00", "Opening hours: 11.00 - 22.00", "Opening hours: 10.00 - 22.00", "Opening hours: 12.00 - 15.00, 17.00 - 23.00", "Opening hours: 10.30 - 23.45"]
var address = ["221, Burwood Highway, Level 1 - BC Building, Burwood 3125","172 Burwood Hwy, Burwood 3151","Box Hill Central, 69/1 Main St, Box Hill VIC 3128","279-281 Burwood Highway, Burwood VIC 3125","251A Burwood Hwy, Burwood VIC 3125"]

var descRestaurant = ["When you want good food, fine wine, and staff who really care about ensuring you have the very best experience, head to Mrs Robinson. Mrs Robinson is open Monday to Thursday from 7am till 6pm, and Friday 7am till 9pm. The café operates all day, with an exclusive a la carte lunch menu from 12pm – 3pm. You are advised to book for lunch, as the restaurant fills up quickly.","The home of Peri-Peri Chicken! Can’t get enough of PERi-PERi? That’s because of its fiery Southern African spirit. What makes it so fiery? Sit back, we’ll show you…","Exciting food. Serve for life.Stomach churning, mouth watering and lip licking actions: This is the NeNe effect.","Take your taste buds on a trip with the best Italian food in Salem, OR. Geppetto's Italian Restaurant & Bar offers friendly service and the delicious flavors of Italy. Our appetizers and entrees are perfect to indulge in for lunch or dinner.",""]

var restIndex = 0


class restaurantTableViewController: UITableViewController {
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*let databaseRef = Database.database().reference()
        
        
        databaseRef.child("Categories").queryOrderedByKey().observe(.childAdded, with: {
            (snapshot) in
            let snapshotValue = snapshot.value as? NSDictionary
            
            let label = snapshotValue?["label"]as? String
            let rating = snapshotValue?["rating"]as? String
            let openHrs = snapshotValue?["openHrs"] as? String
            
            self.posts.insert(postStruct(label: label,rating: rating ,openHrs: openHrs), at: 0)
            
            self.tableView.reloadData()
            
            ref = Database.database().reference()
             databaseHandle = ref?.child("Categories").observe(.childAdded, with: {(snapshot) in
             
             let post = snapshot.value as? String
             if let actualPost = posts {
             self.posts.append(postStruct.)
             
             self.tableView.reloadData()
             }*/
        
            
        }
        
        //post()
    
    
    /*func post() {
        
        let label = "Nando's"
        let rating = "★★★★"
        let openHrs = "Opening hours: 11.00 - 22.00"
        
        let post : [String : AnyObject] = ["label" : label as AnyObject, "rating" : rating as AnyObject , "openHrs" : openHrs as AnyObject]
        
        let databaseRef = Database.database().reference()
        //databaseRef.child("Categories").childByAutoId().setValue(post)
        
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return nameList.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restList", for: indexPath) as! restaurantMenuTableViewCell
        
        // Configure the cell...
        cell.nameLabel?.text = nameList[indexPath.row]
        
        
        //text for restaurant ratings
        cell.starLable?.text = starList[indexPath.row]
        //text for restaurant opening hours
        cell.openinghrsLabel?.text = openHrsList[indexPath.row]
        //image for restaurant
        cell.imgList?.image = UIImage(named: restImage[indexPath.row])

        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        restIndex = indexPath.row
        
        
    }
    
    
}
