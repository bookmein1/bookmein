//
//  profileViewController.swift
//  BookMeIn
//

//

import UIKit
import FirebaseAuth

class profileViewController: UIViewController, UITableViewDelegate{

    
    @IBOutlet weak var profPict: UIImageView!
    
    @IBOutlet weak var redColumn: UIImageView!
    
    @IBOutlet weak var personName: UILabel!
    
    @IBOutlet weak var bookingTable: UITableView!

    @IBOutlet weak var slideMenu: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profPict.layer.masksToBounds = false
        profPict.layer.cornerRadius = profPict.frame.height/12
        profPict.clipsToBounds = true
        
        
        redColumn.layer.masksToBounds = false
        redColumn.layer.cornerRadius = redColumn.frame.height/12
        redColumn.clipsToBounds = true
        
        if Auth.auth().currentUser != nil {
            personName.text = (Auth.auth().currentUser?.email)!
        } else {
            personName.text = "Login ERROR"
        }
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func alertTheUser(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil);
        alert.addAction(ok);
        present(alert, animated: true, completion: nil);
    }
    
    @IBAction func logout(_ sender: UIButton) {
        if AuthCheck.Instance.logOut() {
            dismiss(animated: true, completion: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let goNextPage = storyBoard.instantiateViewController(withIdentifier: "categoriesPage")
            self.present(goNextPage, animated: true, completion: nil)
        } else {
            alertTheUser(title: "Could Not Sign Out", message: "Could not log out try again later")
        }
    }
    
    @IBOutlet weak var slideMenuView: NSLayoutConstraint!
  
    
    var menuShowing = false
    
    
    @IBAction func slideButton(_ sender: Any) {
        
        if (menuShowing) {
            slideMenuView.constant = -240
        } else {
            slideMenuView.constant = 0
            
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        menuShowing = !menuShowing
        
    }
    
    /*func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        <#code#>
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        <#code#>
    }*/
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
