//
//  registerViewController.swift
//  BookMeIn
//
//


import UIKit
import FirebaseAuth
import FirebaseDatabase

class registerViewController: UIViewController, UITextFieldDelegate {


    
    override func viewDidLoad() {
        super.viewDidLoad()

        columnRed.layer.masksToBounds = false
        columnRed.layer.cornerRadius = columnRed.frame.height/12
        columnRed.clipsToBounds = true
        
        emailAddress.delegate = self
        emailAddress.tag = 0
        emailAddress.returnKeyType = UIReturnKeyType.next
        
        passwordSignUp.delegate = self
        passwordSignUp.tag = 1
        passwordSignUp.returnKeyType = UIReturnKeyType.next
        
        rePassword.delegate = self
        rePassword.tag = 2
        rePassword.returnKeyType = UIReturnKeyType.go
        

        
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var columnRed: UIImageView!

    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var emailAddress: UITextField!
    
    @IBOutlet weak var passwordSignUp: UITextField!
    
    @IBOutlet weak var rePassword: UITextField!
    
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z]).{6,10}$")
        return passwordTest.evaluate(with: password)
    }

    @IBAction func createAccount(_ sender: AnyObject) {
        
        
        let userEmail = emailAddress.text;
        let userPassword = passwordSignUp.text;
        let userRepeatPassword = rePassword.text;
        
        if userEmail != "" && userPassword != "" {
                        if userPassword == userRepeatPassword {
                            if self.isPasswordValid(userPassword!){
                                AuthCheck.Instance.signUp(withEmail: userEmail!, password: userPassword!, loginHandler: { (message) in
                                    
                                    if message != nil {
                                        self.alertTheUser(title: "Problem with creating account", message: message!);
                                    } else {
                                        self.performSegue(withIdentifier: "SignInSegue", sender: nil);
                                    }
                                });
                            } else{
                                alertTheUser(title: "Password Error ", message: "Password must be from 6 to 10 characters and have at least 1 uppercase letter")
                            }
                        } else{
                            alertTheUser(title: "User Password is not the same", message: "Please enter the same password in both fields")
                        }
        }
        else {
            alertTheUser(title: "Email and password are required", message: "Please enter email and password in the text fields")
        }
    }
    
    private func alertTheUser(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil);
        alert.addAction(ok);
        present(alert, animated: true, completion: nil);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
        
    }

}
