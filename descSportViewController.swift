//
//  descSportViewController.swift
//  
//
//  Created by Gwenda Hasna'a on 10/4/17.
//
//

import UIKit
protocol setDateSportValueDelegate {
    func setDate(toValue: String)
}


class descSportViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var descSportImg: UIImageView!
    @IBOutlet weak var descSportName: UILabel!
    @IBOutlet weak var descSportLoc: UILabel!
    @IBOutlet weak var descSportRating: UILabel!
    @IBOutlet weak var descSportOpen: UILabel!
    @IBOutlet weak var descSport: UILabel!
    @IBOutlet weak var descSportDate: UIDatePicker!
    
    @IBAction func descSportDateValue(_ sender: Any) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        descDateSport.text = formatter.string(from: descSportDate.date)
        
        
    }
    
    @IBOutlet weak var descDateSport: UILabel!
    
    @IBOutlet weak var descInputSport: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        descSport.layer.masksToBounds = false
        descSport.layer.cornerRadius = descSport.frame.height/16
        descSport.clipsToBounds = true
        
        
        descSportName.text = sportNames[sportIndex]
        descSportRating.text = sportRatings[sportIndex]
        descSportOpen.text = sportOpen[sportIndex]
        descSportImg.image = UIImage(named: sportImgs[sportIndex])
        descSportLoc.text = sportAdd[sportIndex]
        descSport.text = descSports[sportIndex]
        
    
        descInputSport.delegate = self
        descInputSport.tag = 0
        descInputSport.returnKeyType = UIReturnKeyType.next

        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    var delegate: setDateSportValueDelegate?
    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: descSportDate.date)
        descDateSport.text = strDate
        
        
        if self.delegate != nil
        {
            self.delegate?.setDate(toValue: strDate)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let secondController = segue.destination as! confirmSportViewController
        secondController.peopleString = descInputSport.text!
        secondController.receivedDate = descDateSport.text!
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    

}
