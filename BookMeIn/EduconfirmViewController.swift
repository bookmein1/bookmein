//
//  EduconfirmViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/4/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
import EventKit
import FirebaseAuth


class EduconfirmViewController: UIViewController, setDateEduValueDelegate {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var companyEdu: UILabel!
    @IBOutlet weak var timeEdu: UILabel!
    @IBOutlet weak var eduLocation: UILabel!
    @IBOutlet weak var inputEdu: UILabel!
    @IBAction func okButton(_ sender: Any) {
        
        displayAlert(userMessage: "Thank you for booking.")

        
        
    }
    var peopleString = String()
    var receivedDate:String = ""
    
    @IBAction func addEvent(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .short
        let date = dateFormatter.date(from: receivedDate) //according to date format your date string

        
        let eventStore: EKEventStore = EKEventStore()
        
        eventStore.requestAccess (to: .event) {(granted, error) in
            if (granted) && (error == nil)
            {
                
                print("granted \(granted)")
                print("error \(String(describing: error))")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = "Reminder: " + self.companyEdu.text!
                event.startDate = date!
                event.endDate = date!
                event.notes = "Notes: You have an appointment in " + self.companyEdu.text!
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                }
                catch let error as NSError {
                    print("error: \(error)")
                }
                print ("Save Event")
            }
            else {
                print("error: \(String(describing: error))")
            }
            
        }
    }
    

    
    func displayAlert(userMessage:String)
    {
        
        let alertMessage = UIAlertController(title:"Booking confirmed!", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
 self.navigationController?.popToRootViewController(animated: true)
        alertMessage.addAction(okAction);
        
        present(alertMessage, animated:true, completion:nil);
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let goNextPage = storyBoard.instantiateViewController(withIdentifier: "categoriesPage")
        self.present(goNextPage, animated: true, completion: nil)
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        companyEdu.text = eduNames[eduIndex]
        timeEdu.text = receivedDate
        eduLocation.text = eduAdd[eduIndex]
        
        
        if Auth.auth().currentUser != nil {
            userName.text = (Auth.auth().currentUser?.email)!
        } else {
            userName.text = "Login ERROR"
        }
    }
    
    func setDate(toValue:String) {
        self.timeEdu.text = toValue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
