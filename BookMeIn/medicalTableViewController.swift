//
//  medicalTableViewController.swift
//  BookMeIn
//
//  Created by Gwenda Hasna'a on 10/3/17.
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import UIKit
import Foundation

//struct medTable {
//    var medNameTable : [String]
//    var medRating : [String]
//    var medOpenHrs : [String]
//    }


var  medNameTable = ["Burwood Dental Care", "Me Dental Care","Mount Waverley Dental", "Balwyn Central Medical", "Box Hill Superclinic", "Spectacles Plus", "OPSM Eye Hub", "SunDoctors Bentleigh"]
var  medRating = ["★★★","★★★★★","★★★★", "★★★★★","★★★", "★★★★★","★★★★", "★★★★★"]
var  medOpenHrs = ["Opening Hours: 08.00–19.00","Opening Hours: 09.30–19.00","Opening Hours: 08.30–18.00", "Opening Hours: 08.30–22.00","Opening Hours: 08.00–00.00", "Opening Hours: 09.00–17.00","Opening Hours: 09.00–17.00","Opening Hours: 09.00–19.00" , "Opening Hours: 08.00–17.00"]
var medImages = ["dentalcare","medental","mount","balwyn","boxhill","spec","opsm","sundoc"]
var medAdd = ["356 Burwood Hwy, Burwood VIC 3125","140 Burwood Hwy, Burwood VIC 3125","318 Highbury Rd, Mount Waverley VIC 3149","427 Whitehorse Rd, Balwyn VIC 3103","810 Whitehorse Rd, Box Hill VIC 3128"," Burwood Heights Shopping Centre","174/176 Burwood Rd, Hawthorn VIC 3122","116 Patterson Rd, Bentleigh VIC 3204"]
var medDesc = ["Burwood Dental Centre is a family dental practice and has been providing expert dental care with professional staff, modern equipment, and latest dental procedures and techniques for more than 15 years in the Burwood area of Victoria","Me Dental Care is a modern and friendly dental clinic located in the heart of Burwood. We provide a wide variety of General Dentistry, Cosmetic Dentistry and Specialised Dentistry Treatments to patients of all ages. We also provide Preventative Dental Care, Dentistry for Children, Non-Surgical Gum Therapy, and Dentistry for Pregnant Patients.","Mount Waverley Dental can assist with all general dental enquiries and treatments, including gum disease, white fillings, root canal treatment and more.","","Box Hill Superclinic was set up by GPs with a clear mission of providing holistic care for our community. We set out first by establishing a level of primary care access that had not existed in the area which is opening till late, every day of the year.","","Spectacles Plus is a warm family-owned independent practice servicing the Burwood East community for over 30 years. We pride ourselves on giving personal, professional and individual attention to all of our patients.","Eye Hub is the exciting eyecare concept store in Hawthorn where you’ll find everything in one place, plus innovative interactive testing environments and a dedicated eye health learning zone for kids of all ages.","We offer the highest certainty around the detection and treatment of skin cancer. We stop at nothing to give our patients the best possible care and outcomes at every point in their journey.Our doctors are highly trained and our results are processed in our own specialist pathology laboratory – which is the biggest in Australia and does nothing but skin cancer."]

var medServicesIndex = 0

class medicalTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return medNameTable.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicalList", for: indexPath) as! medicalServiceTableViewCell


        // Configure the cell...
        cell.medName?.text = medNameTable[indexPath.row]
        cell.medRating?.text = medRating[indexPath.row]
        cell.medOpenHrs?.text = medOpenHrs[indexPath.row]

        cell.serviceImg?.image = UIImage(named: medImages[indexPath.row])


        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        medServicesIndex = indexPath.row
        
    }
    
 
}

