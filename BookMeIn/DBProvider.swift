//
//  DBProvider.swift
//  BookMeIn
//
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import Foundation
import FirebaseDatabase

class DBProvider {
    private static let _instance = DBProvider();
    
    static var Instance: DBProvider {
        return _instance;
    }
    
    var dbRef: DatabaseReference {
        return Database.database().reference();
    }
    
    var userRef: DatabaseReference {
        return dbRef.child(Constants.USER);
    }
    
    //request booking
    
    //accept booking
    func saveUser(withID: String, email: String, password: String) {
        let data: Dictionary<String, Any> = [Constants.EMAIL: email, Constants.PASSWORD: password, Constants.isService: true];
        userRef.child(withID).child(Constants.DATA).setValue(data);
    }
}
