//
//  ViewController.swift
//  BookMeIn
//
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController, UITextFieldDelegate {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        columnBox.layer.masksToBounds = false
        columnBox.layer.cornerRadius = columnBox.frame.height/12
        columnBox.clipsToBounds = true
      
        usernameBox.delegate = self
        usernameBox.tag = 0
        usernameBox.returnKeyType = UIReturnKeyType.next
        
        passwordBox.delegate = self
        passwordBox.tag = 1
        passwordBox.returnKeyType = UIReturnKeyType.go

    }
    
    @IBOutlet weak var columnBox: UIImageView!
    @IBOutlet weak var bookLogo: UIImageView!
    
    @IBOutlet weak var usernameBox: UITextField!
    @IBOutlet weak var passwordBox: UITextField!

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //=================
    
    

    @IBAction func signIn(_ sender: UIButton) {
        
        let userEmail = usernameBox.text;
        let userPassword = passwordBox.text;
        
        if userEmail != "" && userPassword != ""
        {
            AuthCheck.Instance.login(withEmail: userEmail!, password: userPassword!, loginHandler: { (message) in
                
                if message != nil {
                    self.alertTheUser(title: "Error: ", message: message!);
                } else {
                    self.performSegue(withIdentifier: "SignInSegue", sender: nil);
                }
            });
        } else {
            alertTheUser(title: "Email and password are required", message: "Please enter email and password in the text fields")
        }
        
 }
    
    @IBAction func signUp(_ sender: UIButton) {
    }
    
    private func alertTheUser(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil);
        alert.addAction(ok);
        present(alert, animated: true, completion: nil);
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    public static func getTopViewController() -> UIViewController?{
        if var topController = UIApplication.shared.keyWindow?.rootViewController
        {
            while (topController.presentedViewController != nil)
            {
                topController = topController.presentedViewController!
            }
            return topController
        }
        return nil}
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return (true)
        
        
    }


}

