//
//  Constants.swift
//  BookMeIn
//
//  Copyright © 2017 BookMeInDeakin. All rights reserved.
//

import Foundation

class Constants {
    
    //DBProvider
    static let USER = "users"
    static let EMAIL = "email"
    static let PASSWORD = "password"
    static let DATA = "data"
    static let isService = "isService"
}
