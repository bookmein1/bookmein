//
//  confirmViewController.swift
//  BookMeIn
//

//

import UIKit
import EventKit
import FirebaseAuth


class confirmViewController: UIViewController, setDateValueDelegate {

    @IBOutlet weak var redSummary: UIImageView!
    @IBAction func okButton(_ sender: Any) {
        
        displayAlert(userMessage: "Thank you for booking.")

    }
    
    @IBOutlet weak var confirmedName: UILabel!
    
    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var timeConfirm: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var noPeople: UILabel!
    
    
    var peopleString = String()
    var receivedDate:String = ""
    
    
    @IBAction func eventCalendar(_ sender: UIButton) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .short
        let date = dateFormatter.date(from: receivedDate) //according to date format your date string
        let eventStore: EKEventStore = EKEventStore()
        
        eventStore.requestAccess (to: .event) {(granted, error) in
            if (granted) && (error == nil)
            {
                
                print("granted \(granted)")
                print("error \(String(describing: error))")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = "Reminder: " + self.companyName.text!
                event.startDate = date!
                event.endDate = date!
                event.notes = "Notes: You have an appointment in " + self.companyName.text!
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                }
                catch let error as NSError {
                    print("error: \(error)")
                }
                print ("Save Event")
            }
            else {
                print("error: \(String(describing: error))")
            }
            
        }
    }
    
    func displayAlert(userMessage:String)
    {
        
        let alertMessage = UIAlertController(title:"Booking confirmed!", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:nil);
        self.navigationController?.popToRootViewController(animated: true)
        alertMessage.addAction(okAction);
        
        present(alertMessage, animated:true, completion:nil);
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let goNextPage = storyBoard.instantiateViewController(withIdentifier: "categoriesPage")
        self.present(goNextPage, animated: true, completion: nil)
        
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        redSummary.layer.masksToBounds = false
        redSummary.layer.cornerRadius = redSummary.frame.height/12
        redSummary.clipsToBounds = true
        
        //data
        companyName.text = nameList[restIndex]
        noPeople.text = peopleString
        timeConfirm.text = receivedDate
        locationLabel.text = address[restIndex]
        
        if Auth.auth().currentUser != nil {
            confirmedName.text = (Auth.auth().currentUser?.email)!
        } else {
            confirmedName.text = "Login ERROR"
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDate(toValue:String) {
        self.timeConfirm.text = toValue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
